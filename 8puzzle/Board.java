import java.util.Iterator;

/**
 * Created by Michelle on 3/12/15.
 * Algorithms Part 1 Assignment 4 -- 8Puzzle
 */
public class Board {
    private int[][] blocks;
    private int dimension;
    private int hamming = 0;
    private int manhattan = 0;
    private int blanki;
    private int blankj;
    public Board(int[][] blocks) {
//        this.blocks = blocks;
        dimension = blocks.length;
        this.blocks = new int[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (blocks[i][j] != 0) {
                    if (blocks[i][j] != (i * dimension + (j + 1))) hamming++;
                    manhattan += Math.abs((blocks[i][j] - 1) / dimension - i);
                    manhattan += Math.abs((blocks[i][j] - 1) % dimension - j);
                    this.blocks[i][j] = blocks[i][j];
                }
                else {
                    blanki = i;
                    blankj = j;
                    this.blocks[i][j] = 0;
                }
            }
        }
    }
    public int dimension() { return dimension; }
    public int hamming() {
        return hamming;
    }

    public int manhattan() {
        return manhattan;
    }

    public boolean isGoal() {
        return hamming() == 0;
    }

    public Board twin() {
        int i;
        if (blanki < dimension - 1)
            i = StdRandom.uniform(
                    blanki + 1, dimension);
        else i = StdRandom.uniform(0, blanki);
        int[][] twin = new int[dimension][dimension];
        for (int k = 0; k < dimension; k++) {
            twin[k] = blocks[k].clone();
        }
        int temp = twin[i][0];
        twin[i][0] = twin[i][1];
        twin[i][1] = temp;
        return new Board(twin);
    }

    private int[][] getBlocks() {
        return blocks;
    }

    public boolean equals(Object y) {
        if (y == null) return false;
        if (y == this) return true;
        if (y.getClass() != this.getClass()) return false;
        Board boardY = (Board) y;
        if (dimension() != boardY.dimension()) return false;
        int[][] blocky = boardY.getBlocks();
        for (int i = 0; i < dimension(); i++) {
            for (int j = 0; j < dimension(); j++) {
                if (this.blocks[i][j] != blocky[i][j]) return false;
            }
        }
        return true;
    }

    public Iterable<Board> neighbors() {
        int[][] blockswap = arrayClone(blocks);
        Queue<Board> neighbor = new Queue<Board>();
        //left neighbor
        if (blanki > 0) {
            blockswap[blanki][blankj] = blockswap[blanki - 1][blankj];
            blockswap[blanki - 1][blankj] = 0;
            neighbor.enqueue(new Board(blockswap));
        }

        //above neighbor
        blockswap = arrayClone(blocks);
        if (blankj > 0) {
            blockswap[blanki][blankj] = blockswap[blanki][blankj - 1];
            blockswap[blanki][blankj - 1] = 0;
            neighbor.enqueue(new Board(blockswap));
        }
        //right neighbor
        blockswap = arrayClone(blocks);
        if (blanki < dimension - 1) {
            blockswap[blanki][blankj] = blockswap[blanki + 1][blankj];
            blockswap[blanki + 1][blankj] = 0;
            neighbor.enqueue(new Board(blockswap));
        }
        //below neighbor
        blockswap = arrayClone(blocks);
        if (blankj < dimension - 1) {
            blockswap[blanki][blankj] = blockswap[blanki][blankj + 1];
            blockswap[blanki][blankj + 1] = 0;
            neighbor.enqueue(new Board(blockswap));
        }
        return neighbor;
    }
    private int[][] arrayClone(int[][] original) {
        int[][] blockswap = new int[original.length][];
        for (int i = 0; i < original.length; i++)
            blockswap[i] = original[i].clone();
        return blockswap;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(dimension + "\n");
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                sb.append(String.format("%2d ", blocks[i][j]));
            }
            sb.append('\n');
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int dimension = in.readInt();
        int[][] blocks = new int[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                blocks[i][j] = in.readInt();
            }
        }
        Board initial = new Board(blocks);
        Queue<Board> neighbor = (Queue<Board>) initial.neighbors();
        StdOut.println(initial);
        Iterator it = neighbor.iterator();
        while (it.hasNext()) {
            StdOut.println(it.next());
        }
    }
}
