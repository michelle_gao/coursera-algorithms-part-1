import java.util.Iterator;

/**
 * Created by Michelle on 3/13/15.
 * Algoithms Part 1 Assignment 4 -- 8Puzzle
 */
public class Solver {
    private Stack<Board> solution = new Stack<Board>();
    private boolean isSolvable = false;
    private int moves;

    public Solver(Board initial) {
        if (initial == null) throw new NullPointerException();
        SearchNode original = new SearchNode(initial);
        original.addParent(null);
        moves = 0;
        if (resolve(original)) {
            isSolvable = true;
        }
        else isSolvable = false;
    }


    public boolean isSolvable() {
        return isSolvable;
    }

    public int moves() {
        if (!isSolvable()) return -1;
        else return moves;
    }

    public Iterable<Board> solution() {
        if (!isSolvable()) return null;
        return solution;
    }

    private class SearchNode implements Comparable<SearchNode> {
        private Board board;
        private int nodemoves;
        private SearchNode parent;
        private SearchNode(Board initial) {
            board = initial;
            nodemoves = 0;
        }
        private void setMoves(int newMoves) {
            nodemoves = newMoves;
        }
        private int getMoves() { return nodemoves; }

        private Board getBoard() {
            return board;
        }
        private void addParent(SearchNode parentNode) {
            parent = parentNode;
        }

        private SearchNode getParent() {
            return parent;
        }

        @Override
        public int compareTo(SearchNode o) {
            int priorityo = o.getMoves() + o.getBoard().manhattan();
            int priority = nodemoves + board.manhattan();
            if (priority > priorityo) return 1;
            if (priority < priorityo) return -1;
            else  {
                if (board.manhattan() > o.getBoard().manhattan()) return 1;
                if (board.manhattan() < o.getBoard().manhattan()) return -1;
            }
            return 0;
        }
    }
    private boolean resolve(SearchNode original) {
        Board board = original.getBoard();
        Board twin = board.twin();
        SearchNode boardNode = original;
        SearchNode twinNode = new SearchNode(twin);
        MinPQ<SearchNode> pqBoard = new MinPQ<SearchNode>();
        MinPQ<SearchNode> pqTwin = new MinPQ<SearchNode>();
        int currentMoves = 0;
        int twinCurrentMoves = 0;
        do {
            if (board.isGoal()) {
                moves = boardNode.getMoves();
                SearchNode temp = boardNode;
                do {
                    solution.push(temp.getBoard());
                    temp = temp.getParent();
                }
                while (temp != null);
                return true;
            }
            if (twin.isGoal()) return false;
            Iterator<Board> it = board.neighbors().iterator();
            currentMoves = boardNode.getMoves();
            Iterator<Board> itTwin = twin.neighbors().iterator();
            twinCurrentMoves = twinNode.getMoves();
            while (it.hasNext()) {
                SearchNode tempNode = new SearchNode(it.next());
                tempNode.setMoves(currentMoves + 1);
                if (boardNode.getParent() == null || !tempNode.getBoard().equals(boardNode.getParent().getBoard()))  {
                    pqBoard.insert(tempNode);
                    tempNode.addParent(boardNode);
                }
            }
            while (itTwin.hasNext()) {
                SearchNode tempTwin = new SearchNode(itTwin.next());
                tempTwin.setMoves(twinCurrentMoves + 1);
                if (twinNode.getParent() == null || !tempTwin.getBoard().equals(boardNode.getParent().getBoard()))  {
                    pqTwin.insert(tempTwin);
                    tempTwin.addParent(twinNode);
                }
            }

            if (!pqBoard.isEmpty()) boardNode = pqBoard.delMin();
            if (!pqTwin.isEmpty()) twinNode = pqTwin.delMin();
            board = boardNode.getBoard();
            twin = twinNode.getBoard();

        } while (!pqBoard.isEmpty() && !pqTwin.isEmpty());
        return false;
    }

    public static void main(String[] args) {
       In in = new In(args[0]);
       int dimension = in.readInt();
       int[][] blocks = new int[dimension][dimension];
       for (int i = 0; i < dimension; i++) {
           for (int j = 0; j < dimension; j++)
               blocks[i][j] = in.readInt();
       }
       Board board = new Board(blocks);
       Solver solver = new Solver(board);
       if (!solver.isSolvable()) {
           StdOut.println("No solution possible");
           StdOut.println("moves = " + solver.moves());
       }
       else  {
           Iterable<Board> solution = solver.solution();
           Iterator<Board> it = solution.iterator();
           StdOut.println("Minimum number of moves = " + solver.moves());
           while (it.hasNext()) {
               StdOut.println(it.next());
           }
       }
    }

}
