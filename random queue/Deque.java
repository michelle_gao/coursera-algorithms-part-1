import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by Michelle Gao on 3/5/15.
 * Algorithms part1 assignment 2
 */
public class Deque<Item> implements Iterable<Item> {
    private Node<Item> first;
    private Node<Item> last;
    private int size;
    public Deque() {
        first = null;
        last = null;
        size = 0;
    }
    private Node<Item> getLast() {
        return last;
    }
    public boolean isEmpty() {
        return first == null;
    }
    public int size() {
        return size;
    }

    public void addFirst(Item item) {
        if (item == null) throw new NullPointerException();
        Node<Item> current = first;
        Node<Item> newNode = new Node<Item>();
        newNode.setValue(item);
        newNode.next = current;
        first = newNode;
        if (current != null) current.prev = newNode;
        else last = first;

    size++;
    }

    public void addLast(Item item) {
        if (item == null) throw new NullPointerException();
        Node<Item> newNode = new Node<Item>();
        newNode.setValue(item);
        Node<Item> current = last;
        newNode.setPrev(current);
        last = newNode;
        if (current != null) current.setNext(last);
        else first = last;
        size++;
    }

    public Item removeFirst() {
        if (isEmpty()) throw new NoSuchElementException();
        Node<Item> removeItem = first;
        if (first.getNext() == null) {
            last = null;
            first = null;
        }
        else {
            Node<Item> newFirst = first.getNext();
            newFirst.setPrev(null);
            first = newFirst;
            removeItem.setNext(null);
        }
        size--;
        return removeItem.getValue();
    }

    public Item removeLast() {
        if (isEmpty()) throw new NoSuchElementException();
        Node<Item> removelast = last;
        Node<Item> secondlast = last.getPrev();
        if (secondlast == null) {
            first = null;
            last = null;
        }
        else {
            secondlast.setNext(null);
            last = secondlast;
        }
        size--;
        removelast.setPrev(null);
        return removelast.getValue();
    }

    public Iterator<Item> iterator() {
        return new ListIterator();

    }

    private class ListIterator implements Iterator<Item>
    {
        private Node<Item> current = first;
        public boolean hasNext() { return current != null; }
        public void remove() {
            throw new UnsupportedOperationException();
        }
        public Item next() {
            if (current == null) throw new NoSuchElementException();
            Item item = current.getValue();
            current = current.getNext();
            return item;
        }
    }



    private class Node<Item> {
        private Item value;
        private Node<Item> next;
        private Node<Item> prev;

        public Node() {
            next = null;
            prev = null;
        }

        public Item getValue() {
            return value;
        }
        public Node<Item> getNext() {
            return next;
        }
        public Node<Item> getPrev() {
            return prev;
        }
        public void setValue(Item item) {
            value = item;
        }
        public void setNext(Node<Item> pnext) {
            next = pnext;
        }
        public void setPrev(Node<Item> pprev) {
            prev = pprev;
        }
    }

    public static void main(String[] args) {
        Deque<Integer> deque = new Deque<Integer>();
        deque.addFirst(1);
        deque.addLast(2);
        deque.addLast(3);
        deque.addFirst(0);
        deque.removeFirst();
        deque.removeLast();
        deque.addFirst(0);
        deque.addLast(3);
        deque.addLast(4);
        Iterator<Integer> it = deque.iterator();
        while (it.hasNext()) {
            StdOut.print(it.next() + "  ");
        }
    }
}
