

/**
 * Created by Michelle on 2/8/15.
 */
public class Subset {
    public static void main(String[] args) {
        String input = StdIn.readLine();
        String[] words = input.split(" ");
        int n = Integer.parseInt(args[0]);
        RandomizedQueue<String> rdq = new RandomizedQueue<String>();
        for (int i = 0; i < words.length; i++) {
            rdq.enqueue(words[i]);
        }
        for (int i = 0; i < n; i++) {
            StdOut.println(rdq.dequeue());
        }
    }

}
