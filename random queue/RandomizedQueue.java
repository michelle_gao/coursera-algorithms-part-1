import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by Michelle on 2/8/15.
 * Algorithms Part 1 assignment 2
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] randomQueue;
    private int size = 0;
    public RandomizedQueue() {
        randomQueue = (Item[]) new Object[2];
    }
    public boolean isEmpty() {
        return size == 0;
    }
    public int size() {
        return size;
    }
    public void enqueue(Item item) {
        if (item == null) throw new NullPointerException();
        size++;
        randomQueue[size-1] = item;
        if (size == randomQueue.length)  resize(2 * randomQueue.length);
    }
    public Item dequeue() {
        if (size < 1) throw new NoSuchElementException();
        int index = StdRandom.uniform(size);
        Item removedItem = randomQueue[index];
        randomQueue[index] = randomQueue[size-1];
        randomQueue[size-1] = null;
        size--;
        if (size == randomQueue.length / 4) resize(randomQueue.length/2);
        return removedItem;
    }

    private void resize(int capacity) {
        assert capacity >= size;
        Item[] newQueue = (Item[]) new Object[capacity];
        for (int i = 0; i < size; i++) {
            newQueue[i] = randomQueue[i];
        }
        randomQueue = newQueue;
    }

    public  Item sample() {
        if (size == 0) throw new NoSuchElementException();
        int index = StdRandom.uniform(size);
        return randomQueue[index];
    }
    public Iterator<Item> iterator() {
        return new RandomQueueIterator();
    }

    private class RandomQueueIterator implements Iterator<Item> {

        private int current = 0;
        private Item[] iterQueue;

        public RandomQueueIterator() {
           if (size > 1) StdRandom.shuffle(randomQueue, 0, size-1);
           iterQueue = randomQueue.clone();
        }
        @Override
        public boolean hasNext() {
            return current < size;
        }

        @Override
        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            return iterQueue[current++];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public static void main(String[] args) {
        RandomizedQueue<Integer> randomQueue = new RandomizedQueue<Integer>();
        randomQueue.enqueue(0);
        randomQueue.enqueue(1);
        randomQueue.enqueue(2);
        randomQueue.dequeue();
        randomQueue.dequeue();
        randomQueue.dequeue();
        StdOut.println("Empty:" + randomQueue.isEmpty());
        randomQueue.enqueue(4);
        randomQueue.enqueue(5);
        randomQueue.enqueue(6);
        StdOut.println("first sample: " + randomQueue.sample());
        StdOut.println("second sample: " + randomQueue.sample());
        randomQueue.enqueue(7);
        randomQueue.enqueue(8);
        Iterator<Integer> it = randomQueue.iterator();
        Iterator<Integer> it2 = randomQueue.iterator();
        int i = 0;
        while (it.hasNext()) {
            StdOut.println("queue " + i + ": " + it.next());
            i++;
        }
        i = 0;
        while (it2.hasNext()) {
            StdOut.println("queue2" + i + ":" + it2.next());
            i++;
        }
    }
}
