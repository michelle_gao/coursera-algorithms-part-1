import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Collections;

/**
 * Created by Michelle.
 * Algorithms part1 assignment 3
 */
public class Fast {
    public static void main(String[] args) {
        StdDraw.setXscale(0, 32378);
        StdDraw.setYscale(0, 32378);
        StdDraw.show(0);
        StdDraw.setPenRadius(0.01);

        String arg0 = args[0];
        In in = new In(arg0);
        int N = in.readInt();
        Point[] plist = new Point[N];

        int x, y;
        for (int i = 0; i < N; i++) {
            x = in.readInt();
            y = in.readInt();
            Point point = new Point(x, y);
            point.draw();
            plist[i] = point;
        }
        StdDraw.show(0);
        StdDraw.setPenRadius();
        ArrayList<Double> slopeIndex = new ArrayList<Double>();
        double slopeTemp;
        Map<Integer, ArrayList<Double>> existedSlopes =
                new HashMap<Integer, ArrayList<Double>>();
        for (int i = 0; i < N-3; i++) {
            existedSlopes.put(i, new ArrayList<Double>());
        }
        for (int p = 0; p < N - 3; p++) {
            Point[] sortPoints = plist.clone();
            Arrays.sort(sortPoints, p + 1, N, plist[p].SLOPE_ORDER);
            int lineCount = 1;
            ArrayList<Point> linePoint = new ArrayList<Point>();
            linePoint.add(sortPoints[p+1]);
            for (int q = p + 2; q < N; q++) {
                if (plist[p].SLOPE_ORDER.compare(sortPoints[q],
                        sortPoints[q-1]) == 0) {

                   boolean slopeExisted = false;
                   int j = 0;
                   double slopepq = plist[p].slopeTo(sortPoints[q]);
                   while (!slopeExisted && j < p) {
                     double slopejp = plist[j].slopeTo(plist[p]);
                     if (slopejp == slopepq) slopeExisted = true;
                     j++;
                   }
                    if (!slopeExisted)  {
                        linePoint.add(sortPoints[q]);
                        lineCount++;
                    }
                }
                 else  {
                    if (lineCount < 3)  {
                        lineCount = 1;
                        linePoint.clear();
                        linePoint.add(sortPoints[q]);
                    }
                    else  {
                        linePoint.add(plist[p]);
                        Collections.sort(linePoint);
                        Point first = linePoint.get(0);
                        Point last = linePoint.get(lineCount);
                        first.drawTo(last);
                        StdDraw.show(0);
                        StdDraw.setPenRadius();
                        for (int cl = 0; cl < lineCount; cl++) {
                            StdOut.print(linePoint.get(cl));
                            StdOut.print(" -> ");
                        }
                        StdOut.println(last);
                        lineCount = 1;
                        linePoint.clear();
                        linePoint.add(sortPoints[q]);
                        slopeTemp = first.slopeTo(last);
                        ArrayList<Double> tempSlope = existedSlopes.get(p);
                        tempSlope.add(slopeTemp);
                        existedSlopes.put(p, tempSlope);
                    }
                }
            }
            if (lineCount >= 3)  {
                linePoint.add(plist[p]);
                Collections.sort(linePoint);
                Point first = linePoint.get(0);
                Point last = linePoint.get(lineCount);
                first.drawTo(last);
                StdDraw.show(0);
                StdDraw.setPenRadius();
                for (int cl = 0; cl < lineCount; cl++) {
                    StdOut.print(linePoint.get(cl));
                    StdOut.print(" -> ");
                }
                StdOut.println(last);
                slopeTemp = first.slopeTo(last);
                ArrayList<Double> tempSlope = existedSlopes.get(p);
                tempSlope.add(slopeTemp);
                existedSlopes.put(p, tempSlope);
            }
//            StdDraw.show(0);
//            StdDraw.setPenRadius();
        }

    }
}
