import java.util.Arrays;

/**
 * Created by Michelle.
 * Algorithms part1 assignment 3
 */
public class Brute {
    public static void main(String[] args) {

        double slopeq,
               sloper,
               slopes;
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.setPenRadius(0.01);
        String filename = args[0];
        In in = new In(filename);
        int N = in.readInt();
        Point[] plist = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            Point p = new Point(x, y);
            p.draw();
            plist[i] = p;
        }

        StdDraw.setPenRadius();
        int pl = plist.length;
        Point[] line = new Point[4];
        for (int i = 0; i < pl-3; i++) {
            for (int q = i+1; q < pl-2; q++) {
               slopeq = plist[i].slopeTo(plist[q]);
               for (int r = q+1; r < pl-1; r++) {
                   sloper = plist[i].slopeTo(plist[r]);
                   if (slopeq == sloper) {
                       for (int s = r+1; s < pl; s++) {
                         slopes = plist[i].slopeTo(plist[s]);
                         if (slopes == slopeq) {
                             line[0] = plist[i];
                             line[1] = plist[q];
                             line[2] = plist[r];
                             line[3] = plist[s];
                             Arrays.sort(line);
                             for (int k = 0; k < 3; k++) {
                                 StdOut.print(line[k] + " -> ");
                             }
                             StdOut.println(line[3]);
                             line[0].drawTo(line[3]);
                         }
                       }
                   }
               }
            }
        }
        StdDraw.setPenRadius();
    }
}
