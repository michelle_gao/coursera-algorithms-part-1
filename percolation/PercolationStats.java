/**
 * Created by Michelle Gao on 3/1/15.
 * Algorithms Part 1
 * Programming Assignment 1
 * Add stdlib.jar and algs4.jar to the java classpath
 * Compile in command line: javac-algs4 PercolationStats.java
 * Run in command line: java-algs4 PercolationStats.java
 */
public class PercolationStats {
    private int T;
    private int N;
    private double[] threshold;

    public PercolationStats(int N, int T) {
       if (N <= 0 || T <= 0) throw new java.lang.IllegalArgumentException();
       this.T = T;
       this.N = N;
       this.threshold = new double[T];
       for (int i = 0; i < T; i++) {
        this.threshold[i] = threshold();
       }
    }

    public double mean() {
        return StdStats.mean(threshold);
    }

    public double stddev() {
        return StdStats.stddev(threshold);
    }

    public double confidenceLo() {
        return (mean() - 1.96 * stddev() / Math.sqrt(T));
    }

    public double confidenceHi() {
        return (mean() + 1.96 * stddev() / Math.sqrt(T));
    }

    private double threshold() {
        int x, y;
        int times = 0;
        Percolation pc = new Percolation(N);
        while (!pc.percolates()) {
            x = StdRandom.uniform(N) + 1;
            y = StdRandom.uniform(N) + 1;
            if (!pc.isOpen(x, y)) {
                pc.open(x, y);
                times++;
            }
        }
        return ((double) times) / ((double) (N * N));
    }

    public static void main(String[] args) {
        Stopwatch sw = new Stopwatch();
        int size = Integer.parseInt(args[0]);
        int exs = Integer.parseInt(args[1]);
        PercolationStats ps = new PercolationStats(size, exs);
        StdOut.println("mean:                    = " + ps.mean());
        StdOut.println("stddev:                  = " + ps.stddev());
        StdOut.print("95% confidence interval: = ");
        StdOut.printf("%.17f", ps.confidenceLo());
        StdOut.print(" , ");
        StdOut.printf("%.17f\n", ps.confidenceHi());
        StdOut.println("ElapsedTime:  "+ sw.elapsedTime());
    }

}
