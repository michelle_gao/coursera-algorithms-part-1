/**
 * Created by Michelle on March/1/15.
 * Algorithms part 1
 * Programming Assignment 1
 * Run Percolation directly in the IDE
 * Add library stdlib.jar and algs4.jar to Java classpath
 * Compile in command Line: javac-algs4 Percolation.java
 * Run in command Line: java-algs4 Percolation
 */

public class Percolation {

    private WeightedQuickUnionUF qf;
    private int size;
    private boolean[] isOpen;
    private boolean[] connectTop;
    private boolean[] connectBottom;
    private boolean isPercolate;

    public Percolation(int N) {
        if (N <= 0) throw new IllegalArgumentException();
        size = N;
        isOpen = new boolean[N * N + 1];
        connectTop = new boolean[N * N + 1];
        connectBottom  = new boolean[N * N + 1];
        isPercolate = false;
        qf = new WeightedQuickUnionUF(N * N + 1);
    }

    public void open(int i, int j) {
        validate(i, j);
        int site = xyTo1D(i, j);
        boolean leftrt = false, rightrt = false, uprt = false, downrt = false;
        boolean leftrb = false, rightrb = false, uprb = false, downrb = false;
        if (!isOpen(i, j)) {
            isOpen[site] = true;

            //check and union with left  neighbor
            if (j != 1) {
                int leftneighbor = xyTo1D(i, j - 1);
                if (isOpen[leftneighbor]) {
                    leftrt = connectTop[qf.find(leftneighbor)];
                    leftrb = connectBottom[qf.find(leftneighbor)];
                    qf.union(site, leftneighbor);
                }
            }

            //check  and union with right neighbor
            if (j != size) {
                 int rightneighbor = xyTo1D(i, j + 1);
                 if (isOpen[rightneighbor])  {
                     rightrt = connectTop[qf.find(rightneighbor)];
                     rightrb = connectBottom[qf.find(rightneighbor)];
                     qf.union(site, rightneighbor);
                 }
            }
            if (i != 1) {
                int upneighbor = xyTo1D(i - 1, j);
                if (isOpen[upneighbor]) {
                    uprt = connectTop[qf.find(upneighbor)];
                    uprb = connectBottom[qf.find(upneighbor)];
                    qf.union(site, upneighbor);
                }
            }
            if (i != size) {
                int downneighbor = xyTo1D(i + 1, j);
                if (isOpen[downneighbor]) {
                    downrt = connectTop[qf.find(downneighbor)];
                    downrb = connectBottom[qf.find(downneighbor)];
                    qf.union(site, downneighbor);
                }
            }
            int root = qf.find(site);
            connectTop[root] = downrt || uprt || leftrt || rightrt;
            connectBottom[root] = downrb || uprb || leftrb || rightrb;
            if (i == 1) connectTop[root] = true;
            if (i == size) connectBottom[root] = true;
            if (connectTop[root] && connectBottom[root]) isPercolate = true;
        }

    }

    public boolean isOpen(int i, int j) {
        validate(i, j);
        int site = xyTo1D(i, j);
        return isOpen[site];
    }

    public boolean isFull(int i, int j) {
        validate(i, j);
        int site = xyTo1D(i, j);
        return connectTop[qf.find(site)];
    }

    public boolean percolates() {
        return isPercolate;
    }

    private int xyTo1D(int x, int y) {
        return (x - 1) * size + y;
    }

    private void validate(int i, int j) {
        if (i < 1 || i > size || j < 1 || j > size)
            throw  new IndexOutOfBoundsException();
    }

    public static void main(String[] args) {
        int size = 599;
        Percolation pl = new Percolation(size);
        int i, j, count = 0;
        pl.open(2, 1);
        StdOut.println(pl.isFull(1, 1));
        while (!pl.percolates()) {
            i = StdRandom.uniform(size) + 1;
            j = StdRandom.uniform(size) + 1;
            if (!pl.isOpen(i, j)) {
                pl.open(i, j);
                count++;
            }
        }
        StdOut.println("opened sites: " + count);
        StdOut.print("Threshold: ");
        StdOut.printf("%.6f\n", (double) count/(double) (size*size));
    }
}